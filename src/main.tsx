import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import {createBrowserRouter, Outlet, RouterProvider} from "react-router-dom";
import Error from "./routes/error.tsx";
import RealEstate from "./routes/real-estate.tsx";
import ConsumerPrice from "./routes/consumer-price.tsx";
import "flowbite";
import {store} from "./redux/store";
import {Provider} from "react-redux";
import Home from "./routes/home.tsx";
import AppLayout from "./components/AppLayout.tsx";
import Comparison from "./routes/comparison.tsx";
import Gdp from "./routes/gdp.tsx";

const router = createBrowserRouter([
    {
        path: "/",
        element: <AppLayout><Outlet/></AppLayout>,
        errorElement: (
            <AppLayout>
                <Error/>
            </AppLayout>
        ),
        children: [
            {
                index: true,
                element: <Home/>,
            },
            {
                path: "comparison",
                element: <Comparison/>,
            },
            {
                path: "real-estate",
                element: <RealEstate/>,
            },
            {
                path: "consumer-price",
                element: <ConsumerPrice/>,
            },
            {
                path: "gdp",
                element: <Gdp/>,
            },
        ],
    },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </React.StrictMode>,
);
