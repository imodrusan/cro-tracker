export interface Dict<T> {
    [Key: string]: T;
}