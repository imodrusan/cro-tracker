export type Element = {
    x: string | number | null;
    y: string | number | null;
};

export type ChartSeries = {
    name: string;
    color: string;
    data: Element[];
};
