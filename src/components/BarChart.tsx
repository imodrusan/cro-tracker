import React, {memo} from 'react';
import 'chart.js/auto';
import {Chart} from 'react-chartjs-2';
import ZoomPlugin from "chartjs-plugin-zoom";
import {useAppSelector} from "../redux/hooks.ts";
import {RootState} from "../redux/store.ts";
import {ChartSeries} from "../types/chart.ts";
import Spinner from "./Spinner.tsx";
import 'chartjs-adapter-date-fns';

type AreaChartProps = {
    title?: string;
    series: ChartSeries[];
    isLoading: boolean;
};


const BarChart = memo(({title, series, isLoading}: AreaChartProps) => {
    const {darkMode} = useAppSelector((state: RootState) => state.darkMode);

    if (isLoading) {
        return <Spinner/>
    }

    const titleColor = darkMode ? "#F3F4F6" : "#2f3641";
    const borderColor = darkMode ? "#F3F4F6" : "#313844";
    const labelColor = darkMode ? "#9ba2b0" : "#3b3c3f";

    return <Chart
        type="bar"
        plugins={[ZoomPlugin]}
        options={{
            responsive: true,
            maintainAspectRatio: false,

            plugins: {
                tooltip: {
                    intersect: false,
                    axis: "x"
                },
                zoom: {
                    limits: {
                        x: {min: 'original', max: 'original', minRange: 7},
                    },
                    pan: {
                        enabled: true,
                        mode: 'x',
                    },
                    zoom: {
                        /*
                        onZoomComplete: function ({chart}) {
                            console.log(`I was zoomed!!!`);
                        },
                        */
                        wheel: {
                            enabled: true,
                            modifierKey: 'ctrl'
                        },
                        pinch: {
                            enabled: true
                        },
                        mode: 'x',
                    }
                },
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: title,
                    font: {
                        size: 24,
                    },
                    color: titleColor
                },
            },
            interaction: {
                mode: "index",
                axis: 'x',
                intersect: false
            },
            scales: {
                x: {
                    type: 'time',
                    time: {
                        unit: 'day',
                        tooltipFormat: 'yyyy-MM',
                    },
                    ticks: {
                        font: {
                            size: 14
                        },
                        color: labelColor,
                        autoSkip: true,
                        maxTicksLimit: 5,
                        callback: function (val) {
                            return this.getLabelForValue(val);
                        },
                    },
                    grid: {
                        display: false
                    }
                },
                y: {
                    ticks: {
                        font: {
                            size: 14
                        },
                        color: labelColor,
                    },
                    grid: {
                        lineWidth: 1,
                        color: borderColor,
                        drawTicks: true,
                        tickColor: "transparent"
                    },
                    border: {
                        dash: [2, 4],
                    },
                }
            }
        } as any}
        data={{
            labels: series.length ? series[0].data.map(el => el.x) : [],
            datasets: series.map(dataset => ({
                label: dataset.name,
                data: dataset.data,
                borderColor: dataset.color,
                backgroundColor: dataset.color,
            }))
        }}
    />;
})

export default BarChart