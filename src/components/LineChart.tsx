import React, {memo, useRef, useState} from 'react';
import ZoomPlugin from "chartjs-plugin-zoom";
import {useAppSelector} from "../redux/hooks.ts";
import {RootState} from "../redux/store.ts";
import {ChartSeries} from "../types/chart.ts";
import Spinner from "./Spinner.tsx";
import CrosshairPlugin from 'chartjs-plugin-crosshair';
import 'chartjs-adapter-date-fns'
import {
    BarElement,
    CategoryScale,
    Chart as ChartJS,
    Legend,
    LinearScale,
    LineController,
    LineElement,
    LogarithmicScale,
    PointElement,
    TimeScale,
    Title,
    Tooltip
} from 'chart.js';
import {Chart,} from 'react-chartjs-2';
import {FullScreen, useFullScreenHandle} from "react-full-screen";
import useIsMobile from "../hooks/useIsMobile.ts";

ChartJS.register(LinearScale, LogarithmicScale, Title, CategoryScale, BarElement, PointElement, LineElement, Legend, Tooltip, CrosshairPlugin, ZoomPlugin, TimeScale, LineController);


type AreaChartProps = {
    title?: string;
    series: ChartSeries[];
    isLoading: boolean;
    yValueCallback?: (value: ChartSeries) => string;
    enableLogarithmicScale?: boolean
}


const LineChart = memo(({title, series, isLoading, enableLogarithmicScale, yValueCallback}: AreaChartProps) => {
    const isMobile = useIsMobile();

    const screen = useFullScreenHandle();
    const chartContainerHeight = screen.active ? "100vh" : isMobile ? "340px" : "500px"

    const chartRef = useRef(null);
    const {darkMode} = useAppSelector((state: RootState) => state.darkMode);
    const [logarithmicScale, setLogarithmicScale] = useState<boolean>(false)


    const titleColor = darkMode ? "#F3F4F6" : "#2f3641";
    const borderColor = darkMode ? "#F3F4F6" : "#313844";
    const labelColor = darkMode ? "#9ba2b0" : "#3b3c3f";

    return <FullScreen handle={screen}>
        <div style={{height: chartContainerHeight}}
             className={`relative overflow-hidden bg-gray-50 sm:py-4 sm:px-2 px-1 max-w-xxl text-gray-900 ${screen.active ? 'py-6' : 'py-2 rounded-lg border border-gray-200 shadow dark:border-gray-600'} dark:bg-gray-800 dark:text-white`}>
            {isLoading ? <Spinner/> :
                <Chart
                    type="line"
                    ref={chartRef}
                    options={{
                        responsive: true,
                        maintainAspectRatio: false,
                        plugins: {
                            tooltip: {
                                intersect: false,
                                axis: "x",
                                callbacks: {
                                    // https://www.chartjs.org/docs/latest/configuration/tooltip.html#tooltip-callbacks
                                    label: function (context) {
                                        let label = context.dataset.label || '';

                                        if (label) {
                                            label += ': ';
                                        }

                                        const value = context.parsed.y
                                        if (value !== null) {
                                            label += yValueCallback ? yValueCallback(value) : value;
                                        }
                                        // new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(context.parsed.y)
                                        return label;
                                    }
                                }

                            },
                            crosshair: {
                                line: {
                                    color: borderColor,
                                    width: 2,
                                    dashPattern: [10, 3]
                                },
                                snap: {
                                    enabled: true
                                },
                                sync: {
                                    enabled: false
                                },
                                zoom: {
                                    enabled: false,
                                },
                            },
                            zoom: {
                                limits: {
                                    x: {min: 'original', max: 'original', minRange: 7},
                                },
                                pan: {
                                    enabled: screen.active,
                                    mode: 'x',
                                    onPanStart: () => {
                                        const chart = chartRef.current as any
                                        chart.crosshair.enabled = false;
                                    },
                                    onPanComplete: () => {
                                        setTimeout(() => {
                                            const chart = chartRef.current as any
                                            chart.crosshair.enabled = true;
                                        }, 500)
                                    },
                                },
                                zoom: {
                                    wheel: {
                                        enabled: screen.active,
                                    },
                                    pinch: {
                                        enabled: screen.active
                                    },
                                    mode: 'x',
                                }
                            },
                            legend: {
                                display: false
                            },
                            title: {
                                display: true,
                                text: title,
                                font: {
                                    size: 18,
                                },
                                color: titleColor
                            },
                        },
                        interaction: {
                            mode: "index",
                            axis: 'x',
                            intersect: false
                        },
                        scales: {
                            x: {
                                type: 'time',
                                time: {
                                    tooltipFormat: 'yyyy-MM',
                                },
                                ticks: {
                                    font: {
                                        size: 14
                                    },
                                    color: labelColor,
                                    autoSkip: true,
                                    maxTicksLimit: 5,
                                    callback: function (val) {
                                        return this.getLabelForValue(val);
                                    },
                                },
                                grid: {
                                    display: false
                                }
                            },
                            y: {
                                type: logarithmicScale ? 'logarithmic' : 'linear',
                                display: 'auto',
                                clip: false,
                                ticks: {
                                    font: {
                                        size: 14
                                    },
                                    callback: yValueCallback,
                                    color: labelColor,
                                    maxTicksLimit: isMobile ? 8 : 9
                                },
                                grid: {
                                    lineWidth: 1,
                                    color: borderColor,
                                    drawTicks: true,
                                    tickColor: "transparent"
                                },
                                border: {
                                    dash: [2, 4],
                                },
                            }
                        }
                    } as any}
                    data={{
                        labels: series.length ? series[0].data.map(el => el.x) : [],
                        datasets: series.map(dataset => ({
                            label: dataset.name,
                            data: dataset.data,
                            borderColor: dataset.color,
                            fill: false,
                            pointRadius: series[0].data.length > 40 ? 1 : 4,
                            pointHoverRadius: 6,
                            pointBackgroundColor: dataset.color,
                            pointBorderColor: series[0].data.length > 40 ? "transparent" : borderColor
                        }))
                    }}
                />
            }
            {!screen.active &&
                <button onClick={screen.enter}
                        type="button"
                        className="absolute top-2 sm:right-2 right-1 stroke-gray-800 hover:stroke-black dark:stroke-gray-100 dark:hover:stroke-white hover:bg-gray-200 dark:hover:bg-gray-700 focus:outline-none rounded-lg p-2">
                    <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" fill="none"
                         className="sm:w-5 sm:h-5 h-4 w-4">
                        <polyline points="36 48 48 48 48 36"/>
                        <polyline points="28 16 16 16 16 28"/>
                        <rect x="8" y="8" width="48" height="48"/>
                        <line x1="16" y1="16" x2="28" y2="28"/>
                        <line x1="48" y1="48" x2="36" y2="36"/>
                    </svg>
                    <span className="sr-only">Enter fullscreen</span>
                </button>
            }
            {screen.active &&
                <button onClick={screen.exit}
                        type="button"
                        className="absolute top-2 sm:right-2 right-1 stroke-gray-800 hover:stroke-black dark:stroke-gray-100 dark:hover:stroke-white hover:bg-gray-200 dark:hover:bg-gray-700 focus:outline-none rounded-lg p-2">
                    <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" fill="none"
                         className="sm:w-5 sm:h-5 h-4 w-4">
                        <polyline points="48 36 36 36 36 48"/>
                        <rect x="8" y="8" width="48" height="48"/>
                        <line x1="36" y1="36" x2="48" y2="48"/>
                        <polyline points="16 28 28 28 28 16"/>
                        <line x1="28" y1="28" x2="16" y2="16"/>
                    </svg>
                    <span className="sr-only">Exit fullscreen</span>
                </button>
            }
            {enableLogarithmicScale &&
                <button onClick={() => setLogarithmicScale(!logarithmicScale)}
                        type="button"
                        className="absolute text-[15px] text-gray-400 sm:top-[7px] top-[3px] sm:right-14 right-12 stroke-gray-800 hover:stroke-black dark:stroke-gray-100 dark:hover:stroke-white hover:bg-gray-200 dark:hover:bg-gray-700 focus:outline-none rounded-lg p-2">
                    log
                    <span className="sr-only">Toggle logarithmic scale</span>
                </button>
            }
        </div>
    </FullScreen>
})

export default LineChart
