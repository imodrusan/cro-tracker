import React from 'react';
import {FullScreen, useFullScreenHandle} from "react-full-screen";
import useIsMobile from "../hooks/useIsMobile.ts";

type ChartContainerProps = {
    children: React.ReactNode;
};

const ChartContainer: React.FC<ChartContainerProps> = ({children}) => {
    const screen = useFullScreenHandle();
    const isMobile = useIsMobile();

    const height = screen.active ? "100vh" : isMobile ? "340px" : "500px"

    return (
        <FullScreen handle={screen}>
            <div style={{height}}
                 className={`relative overflow-hidden bg-gray-50 sm:py-4 sm:px-2 px-1 max-w-xxl text-gray-900 rounded-lg border border-gray-200 shadow dark:border-gray-600 py-2 dark:bg-gray-800 dark:text-white`}>
                {children}
                {!screen.active &&
                    <button onClick={screen.enter}
                            type="button"
                            className="absolute sm:top-2 top-1 sm:right-2 right-1 stroke-gray-800 hover:stroke-black dark:stroke-gray-100 dark:hover:stroke-white hover:bg-gray-200 dark:hover:bg-gray-700 focus:outline-none rounded-lg p-2">
                        <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" fill="none"
                             className="sm:w-8 sm:h-8 h-6 w-6">
                            <polyline points="36 48 48 48 48 36"/>
                            <polyline points="28 16 16 16 16 28"/>
                            <rect x="8" y="8" width="48" height="48"/>
                            <line x1="16" y1="16" x2="28" y2="28"/>
                            <line x1="48" y1="48" x2="36" y2="36"/>
                        </svg>
                        <span className="sr-only">Enter fullscreen</span>
                    </button>
                }
                {screen.active &&
                    <button onClick={screen.exit}
                            type="button"
                            className="absolute sm:top-2 top-1 sm:right-2 right-1 stroke-gray-800 hover:stroke-black dark:stroke-gray-100 dark:hover:stroke-white hover:bg-gray-200 dark:hover:bg-gray-700 focus:outline-none rounded-lg p-2">
                        <svg viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg" fill="none"
                             className="sm:w-8 sm:h-8 h-6 w-6">
                            <polyline points="48 36 36 36 36 48"/>
                            <rect x="8" y="8" width="48" height="48"/>
                            <line x1="36" y1="36" x2="48" y2="48"/>
                            <polyline points="16 28 28 28 28 16"/>
                            <line x1="28" y1="28" x2="16" y2="16"/>
                        </svg>
                        <span className="sr-only">Exit fullscreen</span>
                    </button>
                }
            </div>
        </FullScreen>
    );
};

export default ChartContainer;
