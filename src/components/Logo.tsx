function Logo({className}: { className: string }) {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="-145.308 -369.213 887.243 825.66"
            className={className}
        >
            <g
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeMiterlimit="10"
                strokeWidth="15"
                transform="translate(-310.81 -424.094)"
            >
                <path
                    d="M808.978 392.122c0 28.342-5.434 55.409-15.303 80.224-27.854 87.765-109.996 151.368-206.987 151.368-59.954 0-114.232-24.306-153.53-63.592-39.286-39.297-63.591-93.575-63.591-153.53v-5.167h217.121V175h5.167c119.91 0 217.123 97.212 217.123 217.122z"></path>
                <path d="M316.835 642.457H898.748V685.527H316.835z"></path>
                <path d="M316.835 712.782H898.748V764.3620000000001H316.835z"></path>
                <path d="M808.979 327.948L882.077 326.846 730.58 478.343 666.083 448.772 477.906 636.949"></path>
                <path d="M882.077 418.084L882.077 326.846"></path>
            </g>
        </svg>
    );
}

export default Logo;
