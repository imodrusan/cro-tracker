import {Link, useLocation} from "react-router-dom";

function NavigationLink({text, to}: { text: string; to: string }) {
    const location = useLocation();

    const {pathname} = location;

    return (
        <Link
            to={to}
            className={`block py-2 pl-3 pr-4 text-gray-900 rounded dark:border-gray-700 uppercase text-sm ${
                pathname === to
                    ? "bg-gray-100 md:bg-transparent md:text-blue-700 md:p-0 md:dark:text-blue-500 dark:text-white dark:bg-gray-700 md:dark:bg-transparent"
                    : "hover:bg-gray-100 md:hover:bg-transparent md:hover:text-blue-700 md:p-0 md:dark:hover:text-blue-500 dark:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"
            }`}
        >
            {text}
        </Link>
    );
}

export default NavigationLink;
