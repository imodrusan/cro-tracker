import DarkModeToggle from "../components/DarkModeToggle";
import NavigationLink from "../components/NavigationLink";
import React, {useState} from "react";
import {Link} from "react-router-dom";
import useIsMobile from "../hooks/useIsMobile.ts";
import Logo from "../assets/CroTracker.png";

function AppLayout({children}) {
    const isMobile = useIsMobile();
    const [drawerOpened, setDrawerOpened] = useState<boolean>(false)

    return (
        <div className="min-h-screen bg-gray-100">
            <header className="sticky inset-0 z-20">
                <nav
                    className="dark:bg-gray-900 bg-gray-100 w-full top-0 left-0">
                    <div className="max-w-screen-xl flex flex-wrap items-center  mx-auto p-4">
                        <Link to="/" className="flex items-center select-none z-20">
                            <img src={Logo} alt="CroTracker" className="sm:h-[36px] h-8 w-full"/>
                        </Link>
                        <div className="flex md:order-2 ml-auto md:ml-4 z-20">
                            <DarkModeToggle/>
                            {isMobile && <button type="button"
                                                 className="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
                                                 onClick={() => setDrawerOpened(!drawerOpened)}>
                                <span className="sr-only">Open main menu</span>
                                <svg
                                    className="w-5 h-5 dark:stroke-gray-200 stroke-gray-500"
                                    aria-hidden="true"
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 17 14"
                                >
                                    <path
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                        strokeWidth="2"
                                        d="M1 1h15M1 7h15M1 13h15"
                                    />
                                </svg>
                            </button>
                            }
                        </div>
                        {(!isMobile || drawerOpened) &&
                            <div className="items-center justify-between w-full md:flex md:w-auto md:order-1 ml-auto z-20">
                                <ul className="flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 dark:border-gray-700 font-bold">
                                    <li onClick={() => setDrawerOpened(!drawerOpened)}>
                                        <NavigationLink text={"Usporedba"} to={"/comparison"}/>
                                    </li>
                                    <li onClick={() => setDrawerOpened(!drawerOpened)}>
                                        <NavigationLink text={"Cijene"} to={"/consumer-price"}/>
                                    </li>
                                    <li onClick={() => setDrawerOpened(!drawerOpened)}>
                                        <NavigationLink text={"BDP"} to={"/gdp"}/>
                                    </li>
                                    <li onClick={() => setDrawerOpened(!drawerOpened)}>
                                        <NavigationLink text={"Nekretnine"} to={"/real-estate"}/>
                                    </li>
                                </ul>
                            </div>
                        }
                    </div>
                </nav>
                <div className="bg-gradient-to-b from-blue-400 to-transparent dark:from-blue-900 w-full h-full absolute top-0 left-0 z-0 opacity-30"></div>
            </header>
            <main
                className="dark:bg-gray-900"
                style={{minHeight: "calc(100vh - 73px)"}}
            >
                {children}
            </main>
        </div>
    );
}

export default AppLayout;
