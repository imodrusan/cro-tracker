import React from "react";
import Select, {StylesConfig} from "react-select";
import makeAnimated from "react-select/animated";
import {DataSet} from "../redux/api.ts";
import {useAppSelector} from "../redux/hooks.ts";
import {RootState} from "../redux/store.ts";


type SeriesPickerProps = {
    data: DataSet[],
    value: DataSet[]
    isLoading: boolean
    maxSeries?: number
    onChange: (val: DataSet[]) => void,
}


export default function SeriesPicker(props: SeriesPickerProps) {
    const components = makeAnimated()
    const {darkMode} = useAppSelector((state: RootState) => state.darkMode);

    const colourStyles: StylesConfig<DataSet, true> = {
        control: (styles) => ({
            ...styles,
            color: "white",
            background: darkMode ? "#374151" : "#f9fafb",
            border: darkMode ? "2px solid #4b5563" : "2px solid #e5e7eb",
            padding: "4px"
        }),
        option: (styles, {data}) => {
            return {
                ...styles,
                color: data.color.hex,
            };
        },
        menuList: (styles) => {
            return {
                ...styles,
                backgroundColor: darkMode ? "#2d323b" : "#f9fafb",
            };
        },
        multiValue: (styles, {data}) => {
            return {
                ...styles,
                backgroundColor: data.color.hex,
                borderRadius: "8px",
                width: "100%",
            };
        },
        multiValueLabel: (styles) => {
            return {
                ...styles,
                color: "#ffffff"
            };
        },
    };

    const maxSeriesPicked = props.maxSeries && props.value.length >= props.maxSeries


    return (
        <Select
            isMulti
            backspaceRemovesValue={true}
            closeMenuOnScroll={true}
            isSearchable={false}
            options={props.data}
            components={components}
            value={props.value}
            onChange={props.onChange}
            styles={colourStyles}
            isLoading={props.isLoading}
            formatOptionLabel={(option: DataSet) => option.name || option.nameInTotalsChart}
            getOptionValue={(option: DataSet) => option.name || option.nameInTotalsChart}

            isOptionSelected={() => maxSeriesPicked}
            noOptionsMessage={() => maxSeriesPicked ? `Max ${props.maxSeries} series can be selected` : "No options"}
        />
    );
}
