import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {Element} from "../types/chart.ts";


const getRealEstateQuery = (locale: "hr" | "en") => ({
    url: "",
    method: "POST",
    body: JSON.stringify({
        query: `query { dataSets(locales: ${locale}, where: {collection_contains_some: "real-estate"}) { name, color {hex}, index, percentageChange}}`
    })
})


const getConsumerPriceQuery = (locale: "hr" | "en") => ({
    url: "",
    method: "POST",
    body: JSON.stringify({
        query: `query { dataSets(locales: ${locale}, where: {collection_contains_some: "consumer-price"}) { name, color {hex}, index, percentageChange}}`
    })
})

const getGdpQuery = (locale: "hr" | "en") => ({
    url: "",
    method: "POST",
    body: JSON.stringify({
        query: `query { dataSets(locales: ${locale}, where: {collection_contains_some: "gdp"}) { name, color {hex}, index, percentageChange}}`
    })
})

const getTotalQuery = (locale: "hr" | "en") => ({
    url: "",
    method: "POST",
    body: JSON.stringify({
        query: `query { dataSets(locales: ${locale}, where: {collection_contains_some: "total"}) { nameInTotalsChart, color {hex}, index, percentageChange}}`
    })
})


type GraphCmsResponse<T> = {
    data: T
}

export type DataSet = {
    name: string,
    nameInTotalsChart: string
    color: {
        hex: string
    }
    index: Element[]
    percentageChange: Element[]
}

type DatasetResponse = {
    dataSets: DataSet[]
}


export const apiSlice = createApi({
    reducerPath: "api",
    baseQuery: fetchBaseQuery({
        baseUrl: "https://eu-central-1-shared-euc1-02.cdn.hygraph.com/content/clmrzvxko2eqz01t9gjyyhkkn/master",
    }),
    tagTypes: ["api"],
    endpoints: (builder) => ({
        getRealEstateIndexAndPercentageChange: builder.query({
            query: getRealEstateQuery,
            transformResponse: (responseData: GraphCmsResponse<DatasetResponse>) => responseData.data.dataSets
        }),
        getConsumerPriceIndexAndPercentageChange: builder.query({
            query: getConsumerPriceQuery,
            transformResponse: (responseData: GraphCmsResponse<DatasetResponse>) => responseData.data.dataSets
        }),
        getTotalIndex: builder.query({
            query: getTotalQuery,
            transformResponse: (responseData: GraphCmsResponse<DatasetResponse>) => responseData.data.dataSets
        }),
        getGdpIndexAndPercentageChange: builder.query({
            query: getGdpQuery,
            transformResponse: (responseData: GraphCmsResponse<DatasetResponse>) => responseData.data.dataSets
        }),
    }),
});

export const {
    useGetRealEstateIndexAndPercentageChangeQuery,
    useGetConsumerPriceIndexAndPercentageChangeQuery,
    useGetGdpIndexAndPercentageChangeQuery,
    useGetTotalIndexQuery
} = apiSlice;
