import {configureStore} from "@reduxjs/toolkit";
import darkModeReducer from "./darkMode";
import {apiSlice} from "./api.ts";

export const store = configureStore({
    reducer: {
        darkMode: darkModeReducer,
        [apiSlice.reducerPath]: apiSlice.reducer,
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(apiSlice.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
