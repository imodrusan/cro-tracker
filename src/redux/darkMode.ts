import type {PayloadAction} from "@reduxjs/toolkit";
import {createSlice} from "@reduxjs/toolkit";

export interface DarkModeState {
    darkMode: boolean;
}

const initialState: DarkModeState = {
    darkMode: window.matchMedia("(prefers-color-scheme: dark)").matches,
};

export const darkModeSlice = createSlice({
    name: "darkMode",
    initialState,
    reducers: {
        toggleDarkMode: (state: DarkModeState, action: PayloadAction<boolean>) => {
            state.darkMode = action.payload;
        },
    },
});

export const {toggleDarkMode} = darkModeSlice.actions;

export default darkModeSlice.reducer;
