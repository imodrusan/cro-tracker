import React from "react";
import RealEstate from "../assets/RealEstate.png";
import Euro from "../assets/Euro.png";
import Payment from "../assets/Payment.png";
import {Link} from "react-router-dom";


function Home() {
    return (
        <section>
            <div className="dark:bg-gray-900">
                <div className="py-4 px-4 mx-auto max-w-screen-xl text-center lg:py-16 z-10 relative">
                    <h1 className="mb-6 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
                        Visualise, Compare and Analyse
                    </h1>
                    <p className="mb-8 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-200">
                        Track all publicly available data from Croatian Bureau of Statistics
                    </p>
                </div>
            </div>

            <div className="mt-4 px-4 mx-auto max-w-screen-xl lg:px-6 space-y-8">
                <div className="flex justify-center flex-col sm:flex-row gap-2">
                    <Link
                        to={"/real-estate"}
                        className="flex flex-col bg-white dark:bg-gray-800 text-surface shadow-lg dark:bg-surface-dark dark:text-white sm:shrink-0 sm:grow sm:basis-0 rounded">
                        <div className="flex justify-center items-center">
                            <img
                                className="rounded-t-lg sm:rounded-none h-48 pt-8"
                                src={RealEstate}
                                alt="Real Estate"/>
                        </div>
                        <div className="p-6">
                            <h5 className="mb-2 text-xl font-medium leading-tight">Nekretnine</h5>
                            <p className="mb-4 text-base">
                                This is a wider card with supporting text below as a natural
                                lead-in to additional content. This content is a little bit
                                longer.
                            </p>
                            <p className="mb-4 text-base text-surface/75 dark:text-neutral-300">
                                <small>Last updated 3 mins ago</small>
                            </p>
                        </div>
                    </Link>

                    <Link
                        to={"/consumer-price"}
                        className="flex flex-col bg-white dark:bg-gray-800 text-surface shadow-lg dark:bg-surface-dark dark:text-white sm:shrink-0 sm:grow sm:basis-0 rounded">
                        <div className="flex justify-center items-center">
                            <img
                                className="rounded-t-lg sm:rounded-none h-48 pt-8"
                                src={Euro}
                                alt="Prices"/>
                        </div>
                        <div className="p-6">
                            <h5 className="mb-2 text-xl font-medium leading-tight">Cijene</h5>
                            <p className="mb-4 text-base">
                                This card has supporting text below as a natural lead-in to
                                additional content.
                            </p>
                            <p className="mb-4 text-base text-surface/75 dark:text-neutral-300">
                                <small>Last updated 3 mins ago</small>
                            </p>
                        </div>
                    </Link>

                    <Link
                        to={"/gdp"}
                        className="flex flex-col bg-white dark:bg-gray-800 text-surface shadow-lg dark:bg-surface-dark dark:text-white sm:shrink-0 sm:grow sm:basis-0 rounded">
                        <div className="flex justify-center items-center">
                            <img
                                className="rounded-t-lg sm:rounded-none h-48 pt-8"
                                src={Payment}
                                alt="Los Angeles Skyscrapers"/>
                        </div>
                        <div className="p-6">
                            <h5 className="mb-2 text-xl font-medium leading-tight">Bruto domaći proizvod</h5>
                            <p className="mb-4 text-base">
                                This is a wider card with supporting text below as a natural
                                lead-in to additional content. This card has even longer content
                                than the first to show that equal height action.
                            </p>
                            <p className="mb-4 text-base text-surface/75 dark:text-neutral-300">
                                <small>Last updated 3 mins ago</small>
                            </p>
                        </div>
                    </Link>
                </div>

                <div className="mx-auto max-w-screen-md text-center">
                    <p className="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                        Izvor:{" "}
                        <a
                            href="https://podaci.dzs.hr/hr/podaci/cijene/"
                            target="_blank"
                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                            Državni zavod za statistiku
                        </a>
                    </p>
                </div>
            </div>
        </section>
    )
}

export default Home;
