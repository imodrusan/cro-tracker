import React, {useEffect, useState} from "react";
import {DataSet, useGetConsumerPriceIndexAndPercentageChangeQuery} from "../redux/api.ts";
import LineChart from "../components/LineChart.tsx";
import SeriesPicker from "../components/SeriesPicker.tsx";


function RealEstate() {
    const {data, isLoading, isSuccess} = useGetConsumerPriceIndexAndPercentageChangeQuery("hr")
    const [pickedSeries, setPickedSeries] = useState<DataSet[]>([])

    useEffect(() => {
        if (isSuccess) {
            setPickedSeries(data.slice(0, 2))
        }
    }, [data, isSuccess]);


    return (
        <section>
            <div className="py-4 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6 space-y-8">
                <div className="mx-auto max-w-screen-md text-center">
                    <h1 className="mb-6 text-2xl sm:text-3xl font-extrabold tracking-tight leading-none text-gray-900 dark:text-white">
                        Cijene i Inflacija
                    </h1>
                </div>

                <SeriesPicker
                    data={data}
                    value={pickedSeries}
                    onChange={(val: DataSet[]) => setPickedSeries(val)}
                    isLoading={isLoading}
                    maxSeries={3}
                />

                <LineChart
                    title={"Indeksi"}
                    isLoading={isLoading}
                    series={pickedSeries.map(el => ({
                        name: el.name,
                        color: el.color.hex,
                        data: el.index
                    }))}
                    enableLogarithmicScale
                />

                <LineChart
                    title={"Stope promjene"}
                    isLoading={isLoading}
                    series={pickedSeries.map(el => ({
                        name: el.name,
                        color: el.color.hex,
                        data: el.percentageChange
                    }))}
                    yValueCallback={(value) => `${value}%`}
                />
                <div className="mx-auto max-w-screen-md text-center">
                    <p className="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                        Izvor:{" "}
                        <a
                            href="https://podaci.dzs.hr/hr/podaci/cijene/"
                            target="_blank"
                            className="font-medium text-blue-600 dark:text-blue-500 hover:underline">
                            Državni zavod za statistiku
                        </a>
                    </p>
                </div>
            </div>
        </section>
    );
}

export default RealEstate;
