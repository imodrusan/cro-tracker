# CroTracker

Live preview: **https://cro-tracker.netlify.app/consumer-price**

### Technologies

- react
- typescript
- redux toolkit
- vite
- tailwind
- chartJS
- graphCMS with hygraph

## Usage

### Setup

1. clone repo
2. npm install

### Run locally

1. npm dev

### Build

1. npm run build
2. npm preview
